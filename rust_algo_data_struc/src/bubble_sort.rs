pub fn bubble_sort() {
    let list_num:[i32; 10]= [10, 90, 23, 88, 1000, 33, 88, 73, 663, 390];
    println!("{:?}", list_num);

    for i in 0..list_num.len() {
        for j in ((i+1)..list_num.len()).rev(){
            if list_num[j-1] > list_num[j]{
                swap_us(&mut list_num, j-1, j);
            }
            println!("{:?}", list_num);
        } 
    }
}


fn swap_us(m_list: &mut [i32; 10], i: i32, j: i32) {
    let temp: i32;

    temp = m_list[i];
    m_list[i] = m_list[j];
    m_list[j] = temp;
}
