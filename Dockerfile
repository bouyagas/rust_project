FROM rust:alpine

LABEL maintainer="Mohamed Gassama" \
      maintainer_email="bouyagassama@gmail.com" \
      version="1.0"

ENV PROJECT_DIR=/app/rust_project 


WORKDIR $PROJECT_DIR 

RUN apk add --no-cache \
    curl \
    openssh-server \
    vim \
    git 

COPY . $PROJECT_DIR 

CMD ["/bin/sh"]
