// I/O from the rust standard library
use rand::{thread_rng, Rng};
use std::cmp::Ordering::{Equal, Greater, Less};
use std::io::stdin;

fn main() {
    // printin to the standard out
    println!("Guess the number!!!");

    let secret_number: u32 = thread_rng().gen_range(1, 101);
    println!("This is the secret number {}", secret_number);

    loop {
        println!("Please input your guess.");

        // mutable variable that is currently bound to a new, empty
        // instance creating an empty string in the heap memory
        let mut guess = String::new();

        // Calling i/o library
        stdin().read_line(&mut guess).expect("Failed to read line");

        // Converting to from a string into an integer and bound
        // It to a shadowed variable guess
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guess {}", guess);

        // Pattern matching in the results
        match guess.cmp(&secret_number) {
            Less => println!("Too Small!"),
            Greater => println!("Too Big!"),
            Equal => {
                println!("You Win!");
                break;
            }
        }
    }
}
