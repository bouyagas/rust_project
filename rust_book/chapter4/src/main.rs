fn main() {
    let a;
    {
        let b = String::from("Hello World");
        a = &b;
    }
    println!("{}", a);
}

fn get_int_ref() -> &i32 {
    let a = 1;
    &a
}
