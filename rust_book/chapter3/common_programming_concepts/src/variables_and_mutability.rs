pub fn variables_and_mutability() {
    // Variables and Mutability

    // Immutable variable "In Rust variable are immutable by default"
    let i_am_immutable = "Immutable";
    println!("This is a {} variable", i_am_immutable);

    // Mutable variable
    #[allow(unused_mut)]
    let mut i_am_mutable = "Mutable";
    println!("This is a {} variable", i_am_mutable);

    // Constant variable
    const MAX_POINTS: u32 = 100_000;
    println!("This is a {} variable", MAX_POINTS);

    // Variables Shadowing
    // Variable is immutable by default in this case putting mut keyword it will return an error

    let x = 5;
    let x = x + 1; // the second is x = 5 and bound to a shadowed variable let x and x = 6
    let x = x * 2; // the second is now  x = 6 and bound to a shadowed variable let x and x = 12
    println!("This is a shadowed variable {}", x);

    // Another example of variable shadowing
    let spaces = "   ";
    let spaces = spaces.len();
    println!("This is the length of a variable {}", spaces)
}
