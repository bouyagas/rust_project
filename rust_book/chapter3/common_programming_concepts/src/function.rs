// Functions are pervasive in Rust code.
// You’ve already seen one of the most important functions in the language.
// The main function which is the entry point of many programs.
// You’ve also seen the fn keyword, which allows you to declare new functions.
// Rust code uses snake case as the conventional style for function and variable names.
// In snake case, all letters are lowercase and underscores separate words.

// Anatomy of a function ...
// fn -> the function keyword.
// main() -> the function name followed by a parenthese.
// { -> begining of a function body/scoped.
//   -> function body/scoped.
// } -> end of the function body/scoped.

// Public function.
pub fn my_main() {
    println!("Hello, world!");
    // Private scoped/local function.
    another_function();
}

// Global scoped function.
fn another_function() {
    println!("Another function.");
}

// Functions can also be defined to have parameters.
// Which are special variables that are part of a function’s signature.
// When a function has parameters, you can provide it with concrete values for those parameters.
// Technically, the concrete values are called arguments.

// Function with a parameters.
fn func_with_one_params(x: i32) {
    println!("The value of x is {}", x)
}

// Function with two parameters.
fn func_with_two_params(x: i32, y: i32) {
    println!("The value of x is {}", x);
    println!("The value of y is {}", y);
}

// Function Bodies Contain Statements and Expressions.
// Function bodies are made up of a series of statements optionally ending in an expression.
// Because Rust is an expression-based language
// Other languages don’t have the same distinctions
// so let’s look at what statements and expressions are and
// how their differences affect the bodies of functions.

// Statements are instructions that perform some action and do not return a value.
// Function declaration is also a statement
// This function declaration containing one statement
fn my_statement() {
    let y = 6; // The is a statements
}

// Expressions evaluate to a resulting value and
// Function calling is consider an expression
// It can be part of the statement
// Calling a macro is an expression.
// The block that we use to create new scopes, {}, is an expression, for example:
fn my_statement1() {
    let x = 5;
    // This expression.
    let y = {
        let x = 3;
        // Note the x + 1 line without a semicolon at the end.
        // Expressions do not include ending semicolons.
        x + 1
        // If you add a semicolon to the end of an expression, you turn it into a statement.
        //  which will then not return a value.
    };
    println!("The value of y is: {}", y);
}

// Functions with Return Values
fn five() -> i32 {
    5
}

fn calling_five() {
    let x = five(); // It is the same like let x = 5;
    println!("The value of x is: {}", x);
}

fn calling_plus() {
    x = plus_one(6); // It is the same like let x = 6 + 1;
    println!("The value of x: is {}", x)
}

fn plus_one(x: i34) -> i34 {
    x + 1
}
