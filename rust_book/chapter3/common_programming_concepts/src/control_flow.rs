//This is an example of a control flow
//An if expression allows you to branch your code depending on conditions.
//You provide a condition and then state,
//If this condition is met, run this block of code.
//f the condition is not met, do not run this block of code.”
//Blocks of code associated with the conditions in if expressions are sometimes called arms,
//just like the arms in match expressions that we discussed in the
//It’s also worth noting that the condition in this code must be a bool. If the condition isn’t a bool,
//we’ll get an error. For example, try running the following code:

pub fn control_flow() {
    let c: u8 = 12;

    if c > 1 {
        println!("Hello World");
    }
    // It the first condition is false it will check the second expression for a truety
    else if c < 1 {
        println!("Hello")
    }
    // Optionally, we can also include an else expression, which we chose to do here,
    //to give the program an alternative block of code to execute should the condition evaluate to false
    // the program will just skip the if block and move on to the next bit of code.
    else {
        println!("Bye")
    }
}

// Using if in a let Statement
// Because if is an expression, we can use it on the right side of a let statement

pub fn let_if_condition() {
    let x = if 1 + 3 = 4 {
        println!("It is correct")
    } else {
        println!("Not correct")
    };

    println!("The value of x is 4 and it {}", x);
}
