use std::{i128, i32, i64, i8, isize, u128, u32, u64, u8, usize};

#[allow(dead_code)]
pub fn data_types() {
    // In rust there are two type of data types

    //-- Scalar Data Types

    // A scalar type represents a single value.
    //  In Rust there are four scalar types

    // 1) Integer Type
    // An integer is a number without a fractional component
    // By bitss Each signed variant can store numbers from -(2^n - 1) to 2^n - 1 - 1
    // Unsigned variants can store numbers from 0 to 2^n - 1
    // Where n is the number of bits that variant uses
    // Aditionally, the isize and usize types depend on the kind of computer your program is
    // Running on: 64 bits if you’re on a 64-bit architecture
    // And 32 bits if you’re on a 32-bit architecture.

    // So an i8 can store numbers from -(2^7) to 2^7 - 1,
    // -128 to 127 integers in memory
    let eight_bits_signed_min: i8 = i8::MIN;
    let eight_bits_signed_max: i8 = i8::MAX;
    println!(
        "This is an eight signed min is {} integer and max is {} integer",
        eight_bits_signed_min, eight_bits_signed_max
    );

    // So a u8 can store numbers from 0 to 2^8 - 1,
    // 0 to 255 integers in memory.
    let eight_bits_unsigned_min: u8 = u8::MIN;
    let eight_bits_unsigned_max: u8 = u8::MAX;
    println!(
        "This is an eight signed min is {} integer and max is {} integer",
        eight_bits_unsigned_min, eight_bits_unsigned_max
    );

    // So an i32 can store numbers from -(2^31) to 2^31 - 1,
    // −2,147,483,648 to 2,147,483,647 integers in memory
    let thirty_two_bits_signed_min: i32 = i32::MIN;
    let thirty_two_bits_signed_max: i32 = i32::MAX;
    println!(
        "This is thirty two bits signed min is {} integer and max is {} integer ",
        thirty_two_bits_signed_min, thirty_two_bits_signed_max
    );

    // So an u32 can store numbers from 0 to 2^32 - 1,
    // 0 to 4,294,967,295  integers in memory
    let thirty_two_bits_unsigned_min: u32 = u32::MIN;
    let thirty_two_bits_unsigned_max: u32 = u32::MAX;
    println!(
        "This is an thirty two bits unsigned min is {} integer
      and max is {} integer",
        thirty_two_bits_unsigned_min, thirty_two_bits_unsigned_max
    );

    // So an i64 can store numbers from -(2^63) to 2^63 - 1,
    // −9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 integers in memory
    let sixty_four_bits_signed_min: i64 = i64::MIN;
    let sixty_four_bits_signed_max: i64 = i64::MAX;
    println!(
        "This is an sixty four bits signed min is {} integer and max is {} integer",
        sixty_four_bits_signed_min, sixty_four_bits_signed_max
    );

    // So an u64 can store numbers from 0 to 2^64 - 1,
    // 0 to 18,446,744,073,709,551,615 integers in memory
    let sixty_four_bits_unsigned_min: u64 = u64::MIN;
    let sixty_four_bits_unsigned_max: u64 = u64::MAX;
    println!(
        "This is an sixty four bits unsigned min is {} integer and max is {} integer",
        sixty_four_bits_unsigned_min, sixty_four_bits_unsigned_max
    );

    // So an i128 can store numbers from -(2^127) to 2^127 - 1,
    // −170,141,183,460,469,231,731,687,303,715,884,105,728 to
    // 170,141,183,460,469,231,731,687,303,715,884,105,727 integer in memory
    let hundred_and_twenty_eight_bits_signed_min: i128 = i128::MIN;
    let hundred_and_twenty_eight_bits_signed_max: i128 = i128::MAX;
    println!(
        "This is an hundred and twenty eight bits signed min is {} integer and max is {} integer
      ",
        hundred_and_twenty_eight_bits_signed_min, hundred_and_twenty_eight_bits_signed_max
    );

    // So an u128 can store numbers from 0 to 2^128 - 1,
    // 0 to 340,282,366,920,938,463,463,374,607,431,768,211,455 integer in memory
    let hundred_and_twenty_eight_bits_unsigned_min: u128 = u128::MIN;
    let hundred_and_twenty_eight_bits_unsigned_max: u128 = u128::MAX;
    println!(
        "This is an hundred and twenty eight bits unsigned min is {} integer and max is {} integer",
        hundred_and_twenty_eight_bits_unsigned_min, hundred_and_twenty_eight_bits_unsigned_max
    );

    // It for i32 or i64 bit architecture
    let sixty_four_bits_or_thirty_two_bits_architecture_signed_min: isize = isize::min_value();
    let sixty_four_bits_or_thirty_two_bits_architecture_signed_max: isize = isize::max_value();
    println!(
        "This is a sixty four bits or thirty two bits bit architecture signed min is {} and max is {}",
        sixty_four_bits_or_thirty_two_bits_architecture_signed_min, sixty_four_bits_or_thirty_two_bits_architecture_signed_max
    );

    // It for u32 or u64 bit architecture
    let sixty_four_bits_or_thirty_two_bits_architecture_unsigned_min: usize = usize::min_value();
    let sixty_four_bits_or_thirty_two_bits_architecture_unsigned_max: usize = usize::max_value();
    println!(
        "This is a sixty four bits or thirty two bits bit architecture unsigned min is {} and max is {}",
        sixty_four_bits_or_thirty_two_bits_architecture_unsigned_min, sixty_four_bits_or_thirty_two_bits_architecture_unsigned_max
    );

    // decimal
    let decimal_numerical: u32 = 98_222;
    println!("This a decimal {}", decimal_numerical);

    // hexadecimal
    let hexadecimal_numerical: u32 = 0xff;
    println!("This a hexadecimal {}", hexadecimal_numerical);

    // octal
    let octal: u32 = 0o77;
    println!("This is a octal {}", octal);

    // binary
    let bianry: u32 = 0b1111_0000;
    println!("This is a binary {}", bianry);

    // byte
    let byte: u8 = b'A';
    println!("This is a byte {}", byte);

    // 2) Floating-Point Types
    // A floating-point are numbers with decimal points
    // Rust’s floating-point types are f32 and f64,
    // which are 32 bits and 64 bits in size respectively,
    // The default type is f64 because on modern CPUs

    //  The f32 type is a single-precision float
    let thirty_two_floating_point: f32 = 11.0;
    println!(
        "This is thirty two floating point {}",
        thirty_two_floating_point
    );

    // The f64 type is a double-precision float.
    let sixty_four_floating_point: f64 = 12.0;
    println!(
        "This is a sixty four floating point {}",
        sixty_four_floating_point
    );
    // Numeric Operations
    // Rust supports the basic mathematical operations you’d expect for all of the number types

    // Addition
    let sum: u8 = 5 + 10;
    println!("This is an addition {}", sum);

    // Subtraction
    let difference: f32 = 95.5 - 4.3;
    println!("This is an addition {}", difference);

    // Multiplication
    let product: u8 = 4 * 30;
    println!("This is an addition {}", product);

    // Division
    let quotient: f32 = 56.7 / 32.2;
    println!("This is an addition {}", quotient);

    // Remainder
    let remainder: u8 = 43 % 5;
    println!("This is an addition {}", remainder);

    // 3) Boloan Types
    // A Boolean type in Rust has two possible values: true and false
    // Booleans are one byte in size and typed by bool and true is by default
    // The main way to use Boolean values is through conditionals like if statement

    // True
    let is_true: bool = true;
    println!("This is true {}", is_true);

    // False
    let is_false: bool = false;
    println!("This is true {}", is_false);

    // 4) The Character Type
    // Rust’s char type is the language’s most primitive alphabetic type,
    // Char literals are specified with single quote
    // Rust’s char type is four bytes in size and represents a Unicode Scalar Value,

    // Char
    let char_type: char = 'M';
    println!("This is a character type {}", char_type);

    // Emoji
    let heart_eyed_cat: char = '😻';
    println!("This is a emoji {}", heart_eyed_cat);

    //-- Compound Data Types
    // Compound types can group multiple values into one type
    // Rust has two primitive compound types: tuples and arrays.

    // 3) The Tuple Type
    // A tuple is a general way of grouping together a number of values with
    // A variety of types into one compound type.
    // Tuples have a fixed length: once declared,
    // they cannot grow or shrink in size.

    // Tuple
    let tup: (i32, f64, u8, bool, char) = (500, 6.7, 1, true, 'c');
    println!("This is a tuple {:?}", tup);

    // To get the individual values out of a tuple,
    // We can use pattern matching to destructure a tuple value,
    let tup = (500, 6.4, 1);
    #[allow(unused_variables)]
    let (x, y, z) = tup;
    println!("The value of y is: {}", y);

    // In addition to destructuring through pattern matching,
    // we can access a tuple element directly by using a period (.)
    // followed by the index of the value we want to access
    let _five_hundred = tup.0;
    let _six_point_four = tup.1;
    let _one = tup.2;

    // 2) The Array Type
    // Another way to have a collection of multiple values is with an array.
    // Unlike a tuple, every element of an array must have the same type its data
    // is allocated in the stack rather than the heap
    // Arrays in Rust are different from arrays in some other languages
    // Because arrays in Rust have a fixed length, like tuples.
    // In Rust, the values going into an array are
    // Written as a comma-separated list inside square brackets:
    // Arrays are useful when you want your data allocated on the stack rather than the heap
    // When you want to ensure you always have a fixed number of elements.
    // For more growable arrays you can use the vectors
    // Note when you try to access an array that is greater or equal to it length it will return
    // what's knowing as Rust panic

    // Arrays of numbers
    let a = [1, 2, 3, 4, 5, 6];
    println!("This is an array {:?}", a);

    // Example of a fixed array is the month of the year
    // It’s very unlikely that such a program will need to add or remove months,
    // So you can use an array because you know it will always contain 12 elements:
    let months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    println!("THis is a the month of the year {:?}", months);

    // You would write an array’s type by using square brackets
    // Within the brackets include the type of each element, a semicolon,
    // And then the number of elements in the array,
    let a1: [i32; 5] = [1, 2, 3, 4, 5];
    println!("This is array {:?}", a1);

    // An array that contains the same value for each element, you can specify the initial value,
    // Followed by a semicolon, and then the length of the array in square brackets
    let a2 = [3; 5]; // same as let a2 = [3, 3, 3, 3, 3];
    println!("This is array of the same types {:?}", a2);

    // Accessing Array Elements
    let a3 = [1, 2, 3, 4, 5];
    let first = a3[0];
    let second = a3[1];
    println!(
        "This is an {:?} the first index is {} and the second index is {}",
        a3, first, second
    )
}
