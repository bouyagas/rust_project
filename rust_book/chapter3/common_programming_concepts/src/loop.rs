// Repetition with Loops
// It’s often useful to execute a block of code more than once. For this task,
// Rust provides several loops
// Rust has three kinds of loops: loop, while, and for. Let’s try each one.

// Repeating Code with loop
// The loop keyword tells Rust to execute a block of code over
// and over again forever or until you explicitly tell it to stop.
// The symbol ^C represents where you pressed ctrl-c to stop it
// Fortunately, Rust provides another, more reliable way to break out of a loop.
// You can place the break keyword within the loop to tell the program when to stop executing the loop

pub fn infinitive_loop() {
    loop {
        println!("This loop will forever")
    }
}

// Returning Values from Loops
// One of the uses of a loop is to retry an operation you know might fail,
// such as checking whether a thread has completed its job.
// However, you might need to pass the result of that operation to the rest of your code
// To do this, you can add the value you want returned after the break expression you use to stop the loop;

pub fn return_value_for_loop() {
    let mut counter = 0;
    let result = loop {
        counter += 5;
        if counter < 100 {
            break;
        }
    };
    println!("Loop value is {}", resulti);
}

// Conditional Loops with while
// It’s often useful for a program to evaluate a condition within a loop.
// While the condition is true, the loop runs. When the condition ceases to be true
// the program calls break, stopping the loop
//  This loop type could be implemented using a combination of loop, if, else, and break
//  ou could try that now in a program, if you’d like.
//  However, this pattern is so common that Rust has a built-in language construct for it, called a while loop.

pub fn while_loop() {
    let mut count = 3;
    while count != 0 {
        println!("Count {}", count);
        count -= 1;
    }
    println!("OK");
}

// Looping Through a Collection with for
// You could use the while construct to loop over the elements of a collection, such as an array. For example,

pub fn for_loop() {
    let a: [i64; 10] = [1, 3, 39, 43 - 4, -343, 28, 3002, 200000, -388883];
    for element in a.iter() {
        println!("Element is {}", element);
    }
}

pub fn for_loop_reverse_range() {
    for number in (1..20).rev() {
        println!("Number {}", number);
    }
    println!("OK");
}
